An Ingress NGINX Controller inside Kubernetes in order to deploy two different versions of a single PHP application
via their respective URLs


Enable ingress dns and registry in Kubernetes

Build the images
```yaml
docker build -t localhost:32000/php-app:v1 .
docker build -t localhost:32000/php-app:v2 .
```
Push the images to the registry
```yaml
docker push localhost:32000/php-app:v1
docker push localhost:32000/php-app:v2
```

Apply the two deployments
```yaml
microk8s kubectl apply -f deployment_web1.yaml
microk8s kubectl apply -f deployment_web2.yaml
```
Apply the two services
```yaml
microk8s kubectl apply -f service_web1.yaml
microk8s kubectl apply -f service_web2.yaml
```
Apply ingress controller
```yaml
microk8s kubectl apply -f ingress.yaml
```
Point the the host to the info page
```yaml
sudo nano /etc/hosts
```
Type the entry 
```
127.0.0.1 hello-world.info
```